**PHP Version: 5.6.25**

## Installation ##
```
#!bash
git clone https://StefanTotev@bitbucket.org/StefanTotev/trip-sort.git
cd trip-sort

```

## How to run ##
```
#!bash

php index.php
```

## Boarding card format ##
```
#!php

array(
    'start' => 'New York',
    'end' => 'Edinburgh',
    'transport_type' => 'Plane',
    'transport_number' => 'NE343',
    'seat' => '21F',
    'gate' => '3',
    'luggage' => null
)

```

## How to use ##
If you're willing to add boarding cards later, then you don't need to pass an argument to the constructor, otherwise an array in a valid format, showed above, is needed.
```
#!php

$travel = new Travel();
$travel->sort();
echo $travel->display();
```

## How to add a boarding card ##

```
#!php

$travel = new Travel();
$travel->addBoarding(array(
    'start' => 'New York',
    'end' => 'Edinburgh',
    'transport_type' => 'Plane',
    'transport_number' => 'NE343',
    'seat' => '21F',
    'gate' => '3',
    'luggage' => null
));

// ... adding more boarding cards

$travel->sort();
echo $travel->display();
```

## How to add other transportation types ##
All you need is to inherit the abstract class Transport and to implement the getMessage method.
To use the properties passed to the constructor(boarding cards info), you can use getProperty(index) method, as well.

```
#!php

// this will return the seat number if it's available, otherwise should return null
$this->getProperty('seat');
```