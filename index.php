<?php

// Basic autoloader
require_once "autoloader.php";

$cards = require "trips.php";

$trip = new Travel($cards);

// Adding another boarding card for test
$trip->addBoarding(array(
    'start' => 'New York',
    'end' => 'Edinburgh',
    'transport_type' => 'Plane',
    'transport_number' => 'NE343',
    'seat' => '21F',
    'gate' => '3',
    'luggage' => null
));

// Sort the boarding cards
$trip->sort();

// Display guideline
echo $trip->display();