<?php

/**
 * Bus
 */
class Bus extends Transport
{
    const MSG = "Take the airport bus";

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage() {

        $message = static::MSG . parent::MSG_FROM_TO;
        $message .= ( $this->getProperty('seat') === null ) ? parent::MSG_NO_SEAT : parent::MSG_SEAT;

        return sprintf($message, $this->getProperty('start'),
                                $this->getProperty('end'),
                                $this->getProperty('seat'));
    }
}