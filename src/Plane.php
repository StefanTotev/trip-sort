<?php

/**
 * Plane
 */
class Plane extends Transport
{
    const MSG = "From %s, take flight %s to %s.";
    const MSG_GATE_SEAT = " Gate %s, seat %s.";
    const MSG_LUGGAGE_TICKET = "Baggage drop at ticket counter %s.";

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage() {

        $message = static::MSG . static::MSG_GATE_SEAT;
        
        if  ( $this->getProperty('luggage') !== null ) {
            $message .= PHP_EOL . static::MSG_LUGGAGE_TICKET;
        } else {
            $message .= parent::MSG_NO_LUGGAGE;
        }

        return sprintf($message, $this->getProperty('start'),
                                    $this->getProperty('transport_number'),
                                    $this->getProperty('end'),
                                    $this->getProperty('gate'),
                                    $this->getProperty('seat'),
                                    $this->getProperty('luggage'));
    }
}