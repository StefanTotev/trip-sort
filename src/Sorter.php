<?php

/**
 * Sorter
 */
class Sorter
{
    /**
     * Boarding cards
     *
     * @var array
     */
    private $boardingCards = array();

    /**
     * Constructor
     *
     * @param array $list
     */
    public function __construct($list) {
        $this->boardingCards = $list;
        $this->sortList();
    }
    
    private function sortFirstLastCards() {

        // Flags for marking the first and last elements
        $lastBoardingCardFlag = true;
        $firstBoardingCardFlag = true;

        // Loop for passing through all elements
        for ( $i = 0; $i < sizeof($this->boardingCards); $i++ ) {

            // Inner loop for comparing elements each other
            foreach ( $this->boardingCards as $trip ) {

                // If the destination of the current card is a starting point
                // for the next card, we know it is not the last stop
                if ( $this->boardingCards[$i]['end'] == $trip['start'] ) {
                    $lastBoardingCardFlag = false;
                } 
                
                // If the starting point of the current card has previous card
                // we know it's not the first card
                if ( $this->boardingCards[$i]['start'] == $trip['end'] ) {
                    $firstBoardingCardFlag = false;
                }
            }

            // Check the flag for last boarding card
            if ( $lastBoardingCardFlag ) {
                // Putting the last card at the end of array
                $this->boardingCards[] = $this->boardingCards[$i];
                unset($this->boardingCards[$i]);
            }
            
            // Check the flag for first boarding card
            if ( $firstBoardingCardFlag ) {
                // Putting the first card at the beginning of array
                array_unshift($this->boardingCards, $this->boardingCards[$i]);
                unset($this->boardingCards[$i]);
            }
        }

        // Resets array keys
        $this->boardingCards = array_merge($this->boardingCards);
    }

    /**
     * Sort list
     *
     * @return void
     */
    private function sortList() {

        // First we need to reorganise the first and the last trips
        $this->sortFirstLastCards();

        // Loop through all trips
        for ( $i = 0; $i < sizeof($this->boardingCards)-1; $i++ ) {

            // Loop again through all trips for comparing
            for ( $j = $i+1; $j < sizeof($this->boardingCards); $j++ ) {

                // Comparing the starting point of the current card with
                // the ending point of the next one
                if ( $this->boardingCards[$i]['start'] == $this->boardingCards[$j]['end'] ) {
                    // If they're equal, we need to swap their places
                    $temp = $this->boardingCards[$j];
                    $this->boardingCards[$j] = $this->boardingCards[$i];
                    $this->boardingCards[$i] = $temp;
                }
            }
        }
    }

    /**
     * Get sorted list
     *
     * @return void
     */
    public function getSortedList() {
        return $this->boardingCards;
    }
}