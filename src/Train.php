<?php

/**
 * Train
 */
class Train extends Transport
{
    const MSG = "Take train %s";

    /**
     * Get Message
     *
     * @return string
     */
    public function getMessage() {

        $message = static::MSG . parent::MSG_FROM_TO;
        $message .= ( $this->getProperty('seat') !== null ) ? parent::MSG_SEAT : parent::MSG_NO_SEAT;

        return sprintf($message, $this->getProperty('transport_number'),
                                    $this->getProperty('start'),
                                    $this->getProperty('end'),
                                    $this->getProperty('seat'));
    }
}