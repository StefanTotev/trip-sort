<?php

/**
 * Transport
 */
abstract class Transport {

    const MSG_NO_SEAT = "No seat assignment.";
    const MSG_SEAT = "Sit in seat %s.";
    const MSG_FROM_TO = " from %s to %s. ";
    const MSG_LUGGAGE = "Luggage will we automatically transferred from your last leg.";
    const MSG_NO_LUGGAGE = "";
    const MSG_FINAL_DEST = "You have arrived at your final destination.";

    /**
     * Properties
     *
     * @var array
     */
    private $properties = array();

    /**
     * Constructor
     *
     * @param array $properties
     */
    public function __construct(array $properties){
        $this->properties = $properties;
    }

    /**
     * Get Property
     *
     * @param string $idx
     * @return string
     */
    protected function getProperty($idx) {
        return $this->properties[$idx];
    }

    /**
     * Get Message
     *
     * @return string
     */
    abstract public function getMessage();
}