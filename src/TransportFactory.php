<?php

/**
 * Transport Factory
 */
class TransportFactory
{
    /**
     * Call
     *
     * @param string $transportType
     * @param array $properties
     * @return Transport
     */
    public static function call($transportType, array $properties) {

        $transport = ucfirst($transportType);

        if ( class_exists($transport, true) ) {
            
            $instance = new $transport($properties);
            if ( ($instance instanceof Transport) ) {
                return $instance;
            } else {
                throw new Exception("You must to inherit the Transport class!");
            }
        }
        else {
            throw new Exception("This transport is not available.");
        }
    }
}