<?php

/**
 * Travel
 */
class Travel
{
    /**
     * Boarding cards
     *
     * @var array
     */
    private $boardingCards = [];

    /**
     * Sorted boarding cards
     *
     * @var array
     */
    private $sortedBoardingCards = [];

    /**
     * Constructor
     *
     * @param array $cards
     */
    public function __construct($cards = array()) {
        $this->boardingCards = $cards;
    }

    /**
     * Sort
     *
     * @return void
     */
    public function sort() {
        $sorter = new Sorter($this->boardingCards);
        $this->sortedBoardingCards = $sorter->getSortedList();

        // print_r($this->sortedBoardingCards);
    }

    /**
     * Add boarding
     *
     * @param array $card
     * @return void
     */
    public function addBoarding(array $card) {
        $this->boardingCards[] = $card;
    }

    /**
     * Display
     *
     * @return string
     */
    public function display() {

        $message = "";
        $i = 1;
        foreach ( $this->sortedBoardingCards as $boardingCard ) {
            $transportType = strtolower($boardingCard['transport_type']);

            try {
                $transport = TransportFactory::call($transportType, $boardingCard);
            } catch ( Exception $e ) {
                return $e->getMessage();
            }
            
            $message .= sprintf("%d. %s", $i, $transport->getMessage().PHP_EOL);

            if ( sizeof($this->sortedBoardingCards) == $i ) {
                $message .= sprintf("%d. %s", $i+1, Transport::MSG_FINAL_DEST);
            }

            $i++;
        }

        return $message;
    }
}