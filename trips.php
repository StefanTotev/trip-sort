<?php

return array(
    array(
        "start" => "Stockholm",
        "end" => "New York",
        "transport_type" => "Plane",
        "transport_number" => "SK22",
        "seat" => "7B",
        "gate" => "22",
        "luggage" => null
    ),

    array(
        "start" => "Madrid",
        "end" => "Barcelona",
        "transport_type" => "Train",
        "transport_number" => "78A",
        "seat" => "45B",
        "gate" => null,
        "luggage" => null
    ),

    array(
        "start" => "Gerona Airport",
        "end" => "Stockholm",
        "transport_type" => "Plane",
        "transport_number" => "SK455",
        "seat" => "3A",
        "gate" => "45B",
        "luggage" => "334"
    ),

    array(
        "start" => "Barcelona",
        "end" => "Gerona Airport",
        "transport_type" => "Bus",
        "transport_number" => null,
        "seat" => null,
        "gate" => null,
        "luggage" => null
    )
);